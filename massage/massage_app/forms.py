from django import forms
import datetime
from .models import *

durations = session.query(Duration.id, Duration.duration)
duration_choices = tuple(durations)
service_types = session.query(ServiceType.id, ServiceType.service_type)
service_type_choices = tuple(service_types)
service_kinds = session.query(ServiceKind.id, ServiceKind.service_kind)
service_kind_choices = tuple(service_kinds)


class OrderForm(forms.Form):
    duration = forms.ChoiceField(choices=duration_choices)
    service_type = forms.ChoiceField(choices=service_type_choices)
    service_kind = forms.ChoiceField(choices=service_kind_choices)
