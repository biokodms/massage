
# coding: utf-8

import numpy as np
from sqlalchemy import create_engine



engine = create_engine('postgresql://gis:gis@localhost/gis', echo=True)



from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Table, ForeignKey, DateTime, Boolean
from geoalchemy2 import Geometry, Geography
from sqlalchemy.orm import relationship
Base = declarative_base()
from sqlalchemy.orm import sessionmaker
Session = sessionmaker(bind=engine)
session = Session()

'''
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    children = relationship("Child", backref="parent")

     
class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))
'''

class Duration(Base):
    __tablename__ = 'duration'
    id = Column(Integer, primary_key=True)
    duration = Column(String)


class ServiceType(Base):
    __tablename__ = 'service_type'
    id = Column(Integer, primary_key=True)
    service_type = Column(String) #single or double
    pricings = relationship("Pricing", backref="service_type")
    services = relationship("Service", backref="service_type")

 
class ServiceKind(Base):
    __tablename__ = 'service_kind'
    id = Column(Integer, primary_key=True)
    service_kind = Column(String) #sport or classic
    pricings = relationship("Pricing", backref="service_kind")

 
class Pricing(Base):
    __tablename__ = 'pricing'
    id = Column(Integer, primary_key=True)
    price = Column(Integer)
    service_type_id = Column(Integer, ForeignKey('service_type.id'))
    service_kind_id = Column(Integer, ForeignKey('service_kind.id'))
    duration_id = Column(Integer, ForeignKey('duration.id'))


class Service(Base):
    ''' can also be denormalized with ServiceType, ServiceKind and Pricing '''
    __tablename__ = 'service'
    id = Column(Integer, primary_key=True)
    service_type_id = Column(Integer, ForeignKey('service_type.id'))    #single or double
    service_kind_id = Column(Integer, ForeignKey('service_kind.id'))    #sport or classic
    #price = models.IntegerField(null=True)
    duration_id = Column(Integer, ForeignKey('duration.id'))
 
    def get_price(self, session):
        return session.query(Pricing.price).filter(Pricing.service_type_id==self.service_type_id,
                                      Pricing.service_kind_id==self.service_kind_id).first()

    #@login_required
    def book(self, session, client_id, address_id):
        new_order = Order(service_id=self.id, address_id = address_id, client_id=client_id)
        new_order.status_id = session.query(OrderStatus.id).filter(OrderStatus.status=='pending').first()
        session.add(new_order)
        session.flush()
        session.commit()
        return


class Address(Base):
    __tablename__ = 'address'
    id = Column(Integer, primary_key=True)
    street = Column(String)
    street_no = Column(Integer)
    flat_no  = Column(Integer)
    zipcode = Column(String)
    city = Column(String)
    orders = relationship("Order", backref="address")
    
    def get_no_cancelled_orders(self):
        #3. Ile odwołanych rezerwacji było na adres X?
        return self.orders.join(Order.order_status_id).filter(OrderStatus.status==cancelled).count()

Client_Address_association_table = Table('client_address_association', Base.metadata,
    Column('client_id', Integer, ForeignKey('client.id')),
    Column('address_id', Integer, ForeignKey('address.id'))
)

Client_Masseur_association_table = Table('client_masseur_association', Base.metadata,
    Column('client_id', Integer, ForeignKey('client.id')),
    Column('masseur_id', Integer, ForeignKey('masseur.id'))
)

'''
class ParentMtM(Base):
    __tablename__ = 'left'
    id = Column(Integer, primary_key=True)
    children = relationship("ChildMtM",
                    secondary=association_table,
                    backref="parents")

class ChildMtM(Base):
    __tablename__ = 'right'
    id = Column(Integer, primary_key=True)
'''


class Administrator(Base):
    __tablename__ = 'administrator'
    id = Column(Integer, primary_key=True)
    firstname = Column(String)
    lastname = Column(String)
    email = Column(String)


class Client(Base):
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True)
    firstname = Column(String)
    lastname = Column(String)
    phonenumber = Column(String)
    email = Column(String)
    addresses = relationship("Address",
                    secondary=Client_Address_association_table,
                    backref="clients")
    favourites = relationship("Masseur",
                    secondary=Client_Masseur_association_table,
                    backref="clients_who_favourited")
    
     #na adresy może zamawiać usługi, 
     
    #@login_required
    #@service_done_required
    def add_review(masseur, review, session):
        new_review = Review(review=review)
        masseur.reviews.append(new_review)
        session.flush()
        session.commit()
        return
    
    #@login_required
    #@service_done_required
    def add_to_favourites(self, masseur):
        self.favourites.append(masseur)
        session.flush()
        session.commit()
        return
    
    #@login_required
    #@service_done_required
    def rate_maseur(masseur, rating_no, session):
        new_rating = Rating(rating_no=rating_no)
        masseur.ratings.append(new_rating)
        session.flush()
        session.commit()
        return


class Order(Base):
    __tablename__ = 'order'
    id = Column(Integer, primary_key=True)
    order_status_id = Column(Integer, ForeignKey('order_status.id'))
    service_id = Column(Integer, ForeignKey('service.id'))  
    address_id = Column(Integer, ForeignKey('address.id'))
    client_id = Column(Integer, ForeignKey('client.id'))

    #@admin_required
    def cancel(self, session):
        cancelled_id = session.query(OrderStatus.id).filter(OrderStatus.status=='cancelled')
        self.order_status_id = cancelled
        session.flush()
        session.commit()
        return

    #@admin_required
    def assign_masseurs(self, masseurs):
        for masseur in masseurs:
            if masseur in self.service_id.masseur_set:
                self.maseurs.append(masseur)
        #pod warunkiem, ze masażysta świadczy konkretną usługę.
        return


class OrderStatus(Base):
    __tablename__ = 'order_status'
    id = Column(Integer, primary_key=True)
    status = Column(String)
    orders = relationship("Order", backref="order_status")

Masseur_Service_association_table = Table('masseur_service_association', Base.metadata,
    Column('masseur_id', Integer, ForeignKey('masseur.id')),
    Column('service_id', Integer, ForeignKey('service.id'))
)

Masseur_Order_association_table = Table('masseur_order_association', Base.metadata,
    Column('masseur_id', Integer, ForeignKey('masseur.id')),
    Column('order_id', Integer, ForeignKey('order.id'))
)

 
class Masseur(Base):
    __tablename__ = 'masseur'
    id = Column(Integer, primary_key=True)
    firstname = Column(String)
    lastname = Column(String)
    phonenumber = Column(String)
    email = Column(String)
    male = Column(Boolean) #gender
    ratings = relationship("Rating", backref="masseur")
    reviews = relationship("Review", backref="masseur")

    services = relationship("Service",
                    secondary=Masseur_Service_association_table,
                    backref="masseurs")

    orders = relationship("Order",
                    secondary=Masseur_Order_association_table,
                    backref="masseurs")


    #services = models.ManyToManyField(Service,null=True, blank=True)

    def get_clients_who_favourited(self):
        #1. U kogo w ulubionych jest dany usługodawca?
        #return self.client_set
        return self.clients_who_favourited

    def get_average_rating(self, session):
        #2. Jaka jest średnia ocen usługodawcy X?
        return np.average(np.array(session.query(Rating.rating_no).filter_by(
               masseur_id==self.id).all()))


class Rating(Base):
    #can be also done as a blob
    __tablename__ = 'rating'
    id = Column(Integer, primary_key=True)
    rating_no = Column(Integer)
    masseur_id = Column(Integer, ForeignKey('masseur.id'))


class Review(Base):
    #can be also done as a blob
    __tablename__ = 'review'
    id = Column(Integer, primary_key=True)
    review = Column(String)
    masseur_id = Column(Integer, ForeignKey('masseur.id'))


association_table = Table('association', Base.metadata,
    Column('left_id', Integer, ForeignKey('left.id')),
    Column('right_id', Integer, ForeignKey('right.id'))
)

class ParentMtM(Base):
    __tablename__ = 'left'
    id = Column(Integer, primary_key=True)
    children = relationship("ChildMtM",
                    secondary=association_table,
                    backref="parents")

class ChildMtM(Base):
    __tablename__ = 'right'
    id = Column(Integer, primary_key=True)

#Base.metadata.drop_all(bind=engine)
#Base.metadata.create_all(engine)
#session.flush()
#session.commit()

client1_json = {'firstname':'Jan',
               'lastname' : 'Kowalski',
               'phonenumber' : '0123456789',
               'email':'jan.kowalski@gmail.com'}

client2_json = {'firstname':'Andrzej',
               'lastname' : 'Nowak',
               'phonenumber' : '9876543210',
               'email':'andrzej.nowak@gmail.com'}

client_jsons = [client1_json, client2_json]

masseur1_json = {'firstname':'Maciej',
               'lastname' : 'Lipiec',
               'phonenumber' : '6549873210',
               'email':'maciej.lipiec@gmail.com',
               'male': True}

masseur2_json = {'firstname':'Alicja',
               'lastname' : 'Grudzień',
               'phonenumber' : '3210654987',
               'email':'alicja.grudzien@gmail.com',
               'male':False}

masseur_jsons = [masseur1_json, masseur2_json]


address1_json = {'street':'Grudniowa',
                'street_no': 7,
                'flat_no':8,
                'zipcode':'00790',
                'city':'Warszawa'}

address2_json = {'street':'Lipcowa',
                'street_no': 9,
                'flat_no':10,
                'zipcode':'03390',
                'city':'Warszawa'}

address_jsons = [address1_json, address2_json]

pricing1_json = {'service_type':'sportowy',
                 'service_kind':'pojedynczy',
                 'duration': '3-30'}

def set_obj(new_obj, obj_json):
    for key in obj_json.keys():
        setattr(new_obj, key, obj_json.get(key))


def test_populate():
    durations = ['3-30', '45', '60']
    for duration in durations:
        session.add(Duration(duration=duration))

    statuses = ['cancelled', 'awaiting_admin', 'done']
    for status in statuses:
        session.add(OrderStatus(status=status))

    service_types = ['pojedynczy','podwójny'] 
    for service_type in service_types:
        session.add(ServiceType(service_type=service_type))

    service_kinds = ['sportowy','klasyczny'] 
    for service_kind in service_kinds:
        session.add(ServiceKind(service_kind=service_kind))
        
    for obj_json in address_jsons:
        new_obj = Address()
        set_obj(new_obj, obj_json)
        session.add(new_obj)

    for obj_json in masseur_jsons:
        new_obj = Masseur()
        set_obj(new_obj, obj_json)
        session.add(new_obj)

    for obj_json in client_jsons:
        new_obj = Client()
        set_obj(new_obj, obj_json)
        session.add(new_obj)

    session.flush()
    session.commit()
    return
   
#test_populate()

'''
parent = Parent()
child = Child()
session.add(parent)
session.add(child)
session.flush()
session.commit()

child.parent = parent
session.flush()
session.commit()
'''