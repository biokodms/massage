from django.conf.urls import url

from . import views

urlpatterns = [
    #url(r'^$', views.index, name='index'),
    url(r'^order_list', views.order_list, name='order_list'),
    url(r'^order/(?P<pk>\d+)/$', views.available_masseurs_list, name='available_masseurs_list'),
    url(r'^order/(?P<pk>\d+)/$', views.assign_masseur, name='assign_masseur'),
    url(r'^order/(?P<pk>\d+)/$', views.cancel_order, name='cancel_order'),
]