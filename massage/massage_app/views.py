# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib import messages

# Create your views here.

from django.http import HttpResponse
from .models import *
from .forms import OrderForm

from celery import shared_task


@shared_task
def place_order(cleaned_data):
    service_id = session.query(Service.id).filter_by(duration_id==cleaned_data['duration'],
                            service_type_id==cleaned_data['service_type'],
                            service_kind_id==cleaned_data['service_kind']).first()
    awaiting = session.query(OrderStatus.id).filter_by(status=='awaiting_admin').first()
    order = Order(service_id=service_id, status_id=awaiting)
    session.add(order)
    session.flush()
    session.commit()
    return


def order_form(request):
 #if request.user.is_authenticated:
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data        
    else:
        form = OrderForm()
    place_order.delay(cleaned_data)
    messages.success(request, 'We are placing Your order! Wait a moment and refresh this page.')
    return render(request, 'with_sqlalchemy/order_form.html', {'form': form})

#@permission_required('massage_app.assign_masseurs')
def order_list(request):
  #if request.user.is_superuser:
 #if request.user.is_authenticated:

    orders = session.query(Order).all()
    return render(request, 'massage_app/order_list.html', {'orders':orders})


#@permission_required('massage_app.assign_masseurs')
def available_masseurs_list(request, id):
  #if request.user.is_superuser:
    order = session.query(Order).filter_by(id==id).first()
    available_masseurs = order.service_id.masseurs
    return render(request, 'massage_app/available_masseurs_list.html',
                  {'available_masseurs':available_masseurs, 'service':service})


#@permission_required('massage_app.assign_masseurs')
def assign_masseur(request, order_id, masseur_id):
  #if request.user.is_superuser:
    order = session.query(Order).filter_by(id==order_id).first()
    masseur = session.query(Masseur).filter_by(id==masseur_id).first()

    order.assign_masseurs([masseur])
    available_masseurs = order.service_id.masseurs    
    return render(request, 'massage_app/available_masseurs_list.html',
                  {'available_masseurs':available_masseurs, 'service':service})


#@permission_required('massage_app.cancel_order')
def cancel_order(request, id):
 #if request.user.is_superuser:
    order_to_cancel = session.query(Order).filter_by(id==id).first()
    cancel_status_id = session.query(OrderStatus.id).filter_by(status=='cancelled').first()
    order_to_cancel.order_status_id = cancel_status_id
    session.flush()
    session.commit()
    orders = session.query(Order).all()
    return render(request, 'massage_app/order_list.html', {'orders':orders})

'''
def service_list(request):
    #this houses a form
    service_types = session.query(ServiceType).all()
    service_kinds = session.query(ServiceKind).all()
    durations = session.query(Duration).all()
    dict_i = {'service_types' : service_types,
              'service_kinds' : service_kinds,
              'durations' : durations}
    return render(request, 'massage_app/service_list.html', dict_i)
'''

def index(request):
    return HttpResponse("Hello, world. You're at the index.")


def rate_masseur(request):
    return HttpResponse("Congratulations! You rated your massage.")


def write_review(request):
    return HttpResponse("Congratulations! You wrote a review.")


from django.contrib.auth import authenticate, login

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
    else:
        pass
       # Return an 'invalid login' error message.
    return
# get average rating
# get no cancelled orders
# get  